const FlowBabelWebpackPlugin = require('flow-babel-webpack-plugin');
const path = require('path');
const SpeedMeasurePlugin = require('speed-measure-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const DuplicatePackageCheckerPlugin = require('duplicate-package-checker-webpack-plugin');

const smp = new SpeedMeasurePlugin();

const { NODE_ENV } = process.env;

module.exports = smp.wrap({
  mode: NODE_ENV === 'production' ? NODE_ENV : 'development',

  // the place webpack will start when building your bundles
  entry: ['./web/index.js'],

  // sets up rules for any special importers
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
      {
        test: /\.mjs$/,
        include: /node_modules/,
        type: 'javascript/auto',
      },
    ],
  },

  // sets up plugins
  plugins: [
    new FlowBabelWebpackPlugin(),
    new BundleAnalyzerPlugin(),
    new FriendlyErrorsWebpackPlugin(),
    new DuplicatePackageCheckerPlugin(),
  ],

  // file extensions for webpack to look at
  resolve: {
    extensions: ['*', '.js', '.jsx', '.mjs'],

    // alias: {
    //   react: 'preact-compat',
    //   'react-dom': 'preact-compat',
    //   // Not necessary unless you consume a module using `createClass`
    //   'create-react-class': 'preact-compat/lib/create-react-class',
    //   // Not necessary unless you consume a module requiring `react-dom-factories`
    //   'react-dom-factories': 'preact-compat/lib/react-dom-factories',
    // },
  },
  // where webpack will output your finished bundle
  output: {
    path: path.join(__dirname, 'dist'),
    publicPath: '/',
    filename: 'bundle.js',
    pathinfo: true,
  },

  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    hot: true,
    compress: true,
  },
});
