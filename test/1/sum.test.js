import {sum} from './sum';

test('add 1 + 2 to equal 3', () => {
  expect(sum(1, 2)).toBe(3);
});

test('add 1 + "2" to equal 3', () => {
  expect(sum(1, '2')).toBe(3);
});

test('add "1" + "2" to equal 3', () => {
  expect(sum('1', '2')).toBe(3);
});

test('add null + "2" to equal 2', () => {
  expect(sum(null, '2')).toBe(2);
});

test('add undefined + "2" to equal 2', () => {
  expect(sum(undefined, '2')).toBe(2);
});

test('add NaN + "2" to equal 2', () => {
  expect(sum(NaN, '2')).toBe(2);
});
