function sum(a, b) {
  return Number(a || 0) + Number(b || 0);
}
export {sum};
