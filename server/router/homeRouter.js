import Router from 'koa-router';

const homeRouter = new Router();

homeRouter.get('/install', async (ctx) => {
  ctx.state = {
    session: ctx.session,
    title: 'app',
  };
  console.log(ctx.session);
  if (ctx.session.accessToken) {
    ctx.redirect('/');
  }
  await ctx.render('index.html');
});

export default homeRouter;
