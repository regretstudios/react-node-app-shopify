import Router from 'koa-router';
import renderReactApp from '../render-react-app';

const renderReactRouter = new Router();

renderReactRouter.get('/*', renderReactApp);

export default renderReactRouter;
