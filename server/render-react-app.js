import React from 'react';
import { renderToString } from 'react-dom/server';
import HTML, { DOCTYPE } from '@shopify/react-html';
import { StaticRouter } from 'react-router';

import App from '../web/App';

export default (ctx) => {
  const context = {};

  const markup = DOCTYPE
    + renderToString(
      <HTML scripts={[{ path: 'bundle.js' }]}>
        <StaticRouter location={ctx.url} context={context}>
          <App />
        </StaticRouter>
      </HTML>,
    );
  ctx.body = markup;

  // ctx.body = markup;
};
