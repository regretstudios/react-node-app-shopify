import dotenv from 'dotenv';
import Koa from 'koa';
import logger from 'koa-logger';
// import serve from 'koa-static';
import koaBunyanLogger from 'koa-bunyan-logger';
import session from 'koa-session';
import views from 'koa-views';
import createShopifyAuth, { verifyRequest } from '@shopify/koa-shopify-auth';
import koaWebpack from 'koa-webpack';
import graphQLProxy from '@shopify/koa-shopify-graphql-proxy';
import mount from 'koa-mount';
import webpack from 'webpack';
import path from 'path';

import config from '../webpack.config';
import homeRouter from './router/homeRouter';
import renderReactApp from './render-react-app';

dotenv.config();

const { SHOPIFY_SECRET, SHOPIFY_API_KEY } = process.env;

const compiler = webpack(config);
const pView = path.join(__dirname, '/views');

const app = new Koa();

app.use(session(app));
app.use(
  views(pView, {
    extension: 'ejs',
  }),
);
if (process.env.NODE_ENV !== 'production') {
  app.use(logger());
  app.use(koaBunyanLogger());
}

app.keys = [SHOPIFY_SECRET];

console.log(SHOPIFY_SECRET, SHOPIFY_API_KEY);

app.use(homeRouter.routes());
// app.use(serve('dist'));

// app.use(
//   createShopifyAuth({
//     apiKey: SHOPIFY_API_KEY,
//     secret: SHOPIFY_SECRET,
//     scopes: ['write_products'],
//     afterAuth(ctx) {
//       const { shop, accessToken } = ctx.session;

//       console.log('We did it!', shop, accessToken);

//       ctx.redirect('/');
//     },
//   }),
// );

// app.use(mount('/shopify', graphQLProxy()));

// app.use(
//   verifyRequest({
//     authRoute: '/auth',
//     fallbackRoute: '/install',
//   }),
// );

koaWebpack({ compiler }).then((middleware) => {
  app.use(middleware, { serverSideRender: true });
  app.use(renderReactApp);
});

// app.use(renderReactApp);

export default app;
