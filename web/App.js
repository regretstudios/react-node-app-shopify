import React from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import ShopifyRoutePropagator from '@shopify/react-shopify-app-route-propagator';

import HomePage from './Page/HomePage';

const Propagator = withRouter(ShopifyRoutePropagator);

export default function () {
  return (
    <div>
      <Propagator />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route path="/settings">
          <div>
            <h1>Settings</h1>
          </div>
        </Route>
        <Route>
          <div>
            <h1>404</h1>
          </div>
        </Route>
      </Switch>
    </div>
  );
}
