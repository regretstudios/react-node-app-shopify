import React from 'react';

type IProductList = {
  products: [],
};

export default function ProductList({ products = [] }: IProductList) {
  const productItems = products.map(({ title, id }) => <li key={`${id}`}>{title}</li>);

  return <ul>{productItems}</ul>;
}
