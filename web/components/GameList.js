import React from 'react';

import GameItem from './GameItem';

type TGameList = {
  games: [],
  onAddGame: () => void,
};

export default function GameList({ games = [], onAddGame }: TGameList) {
  const gameItems = games.map(game => (
    <GameItem key={game.name} game={game} onAddGame={onAddGame} />
  ));

  return <ul>{gameItems}</ul>;
}
