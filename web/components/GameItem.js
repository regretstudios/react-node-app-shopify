import React from 'react';

function square(n: number): number {
  return n * n;
}

square(2);

type IGameItem = {
  onAddGame: (string, string) => void,
  game: {
    thumbnail: string,
    name: string,
  },
};

export default function GameItem({ onAddGame, game: { thumbnail, name } }: IGameItem) {
  return (
    <li>
      <img src={thumbnail} alt={name} />
      <p>{name}</p>
      <button
        type="button"
        onClick={() => {
          onAddGame(name, thumbnail);
        }}
      >
        Create product
      </button>
    </li>
  );
}
