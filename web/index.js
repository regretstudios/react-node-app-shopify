import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

import App from './App';

const client = new ApolloClient({
  uri: '/shopify/graphql',
  fetchOptions: {
    credentials: 'include',
  },
});

ReactDOM.hydrate(
  <ApolloProvider client={client}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </ApolloProvider>,
  document.getElementById('app'),
);
if (module.hot) {
  module.hot.accept(() => {
    console.log('Accepting the updated printMe module!');
  });
}
